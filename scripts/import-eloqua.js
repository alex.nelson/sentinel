const winston = require('winston');

// use babel compiled code for simplicity
// `npm run build` before using
const EloquaAPIClient = require('../dist/services/Eloqua');

const eloquaHostName = process.env.ELOQUA_HOSTNAME || 'secure.p01.eloqua.com';
const eloquaCompanyName = process.env.ELOQUA_COMPANY;
const eloquaUserName = process.env.ELOQUA_USERNAME;
const eloquaPassword = process.env.ELOQUA_PASSWORD;
const eloquaObjectId = 163;

const eloqua = new EloquaAPIClient(eloquaHostName, eloquaCompanyName, eloquaUserName, eloquaPassword);

// previously converted CSV to JSON
// node_modules/csvtojson/bin/csvtojson ~/Downloads/Appstore+Leads+for+Eloqua.csv > ./scripts/db/leads.json
const LEADS = require('./db/leads.json');

function runImport() {
  eloqua.getObjectFields(eloquaObjectId)
    .then(fields => eloqua.importDataFromStyx(eloquaObjectId, fields, LEADS))
    .then(() => { winston.log('info', 'leads imported'); })
    .catch(err => { winston.log('error', 'Import Eloqua Script', err); });
}

runImport();

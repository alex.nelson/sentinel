# Sentinel Scripts

Sometimes there are needs to run a script locally. These aren't meant to run in production but can be useful for leveraging some of the services built for Sentinel.

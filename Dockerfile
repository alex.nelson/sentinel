FROM node:argon

# Create app directory
RUN mkdir -p /usr/src/app
WORKDIR /usr/src/app

# Install app dependencies
COPY package.json /usr/src/app
RUN npm install --only=production

# Bundle app source
COPY . /usr/src/app

# Set Environment Variables for Dev
ENV LOG_LEVEL debug
ENV APP_VERSION 0.0.3
ENV AWS_BUCKET domo-client-services
ENV AWS_KEY_PREFIX domoapps/service-orders
ENV MONGO_CA_PATH ./static/certs/compose.cert

EXPOSE 3000
CMD ["npm", "run", "serve"]

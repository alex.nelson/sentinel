# Sentinel Service

Backend service for Appstore automation tools written in node.js

## Initial Setup

```
$ cd /dev/dir/you/prefer
$ git clone git@git.empdev.domo.com:appstore-automation/sentinel.git
$ npm install
```

## Usage

### Development

We're taking the Test Driven Development approach to building new models, routes, and services. The basic idea behind this is that you build out your *.spec.js file to validate all of the different features / functions of the module you're about to right. Once you're done with the test cases then you jump into your actual module code and write until the cases pass. Read up on this for more details.

To get started with this you'll want to be sure you've installed [Docker](https://www.docker.com/products/overview) on your machine.

#### Start Local MongoDB for Test & Dev

This command will start your local MongoDB in interactive mode. To stop you just use CTRL-C.

```
$ mongod
```

#### Start TDD

``` 
$ npm run test:watch
```

#### Start Sentinel (in another terminal window)

```
$ npm start
```

#### Build Docker Image

Make sure you've already authenticated to Domo's Docker Registry

    USR: appstore-automation
    PWD: SKDEj5oP1EZzI91G

```
$ npm run docker
```

## Style Guides
- For CSS rules, please see [Reasonable CSS](http://rscss.io/)
- For JS rules, please see [AirBnB's styleguide](https://github.com/airbnb/javascript)

## Existing Endpoints

```
POST /api/auth/login
POST /api/auth/validate
POST /api/updates/leads
ALL /proxy/*
GET /utils/health/sentinel
```

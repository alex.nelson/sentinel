const BasicAPIClient = require('../api-client');
const winston = require('winston');

class StyxAPIClient extends BasicAPIClient {
  constructor(host, username, password) {
    super(host);
    this.username = username;
    this.password = password;

    this.urls = {
      getLeads: () => '/service/apps/leads'
    };
  }

  getNewLeads(timestamp, envs) {
    const startTime = timestamp instanceof Date ? timestamp.getTime() : timestamp;

    const options = {
      method: 'GET',
      url: this.buildURL('getLeads'),
      headers: this.headers,
      auth: { username: this.username, password: this.password },
      qs: { environmentIds: envs.join(), startDate: startTime }
    };

    winston.log('info', 'Styx: get new leads', {
      startTime,
      envs
    });

    return this.request(options)
      .then(res => {
        winston.log('info', 'Styx: Leads returned', { count: res.length });
        return res;
      })
      .catch(err => {
        winston.log('error', 'Error getting Appstore leads from Styx', err);
        throw err;
      });
  }
}

module.exports = StyxAPIClient;

/* eslint-disable no-unused-expressions */
import nock from 'nock';
import sinon from 'sinon';
import { expect } from 'chai';
import Service from './styx-client';

describe('(Service): Styx', () => {
  let service;
  let spy;

  beforeEach(done => {
    service = new Service('styx.net', 'usr', 'pwd');
    spy = sinon.spy(service, 'request');
    done();
  });

  afterEach(done => {
    done();
  });

  it('should instantiate', () => {
    expect(Service).to.be.instanceof(Function);
  });

  describe('.getNewLeads', () => {
    const date = new Date();
    const envs = ['dev3', 'dev4', 'dev5'];

    beforeEach(done => {
      nock('https://styx.net')
        .get('/service/apps/leads')
        .query({ environmentIds: 'dev3,dev4,dev5', startDate: date.getTime() })
        .reply(200);
      done();
    });

    it('should create formatted request', done => {
      service
        .getNewLeads(date, envs)
        .then(res => {
          const args = spy.getCall(0).args[0];
          expect(res).to.exist;
          expect(spy.calledOnce).to.be.true;

          expect(args).to.have.property('method', 'GET');
          expect(args).to.have.property('url', 'https://styx.net/service/apps/leads');
          expect(args).to.have.deep.property('auth.username', 'usr');
          expect(args).to.have.deep.property('auth.password', 'pwd');
          expect(args).to.have.deep.property('qs.environmentIds', envs.join());
          expect(args).to.have.deep.property('qs.startDate', date.getTime());
          done();
        });
    });
  });
});

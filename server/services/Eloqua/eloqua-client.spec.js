/* eslint-disable no-unused-expressions */
import nock from 'nock';
import sinon from 'sinon';
import { expect } from 'chai';
import Service from './eloqua-client';
import AppstoreLead from '../../models/AppstoreLead';
import TestData from '../../../static/testdata/leads.json';

describe('(Service): Eloqua', () => {
  let service;
  let spy;

  beforeEach(done => {
    service = new Service('test.net', 'company', 'usr', 'pwd');
    spy = sinon.spy(service, 'request');
    done();
  });

  afterEach(done => {
    done();
  });

  it('should instantiate', () => {
    expect(Service).to.be.instanceof(Function);
  });

  describe('.getContactByEmail', () => {
    beforeEach(done => {
      nock('https://test.net').get('/api/REST/1.0/data/contacts').reply(200);
      done();
    });

    it('should create a formatted request', done => {
      service.getContactByEmail('test@domo.com').then(() => {
        const args = spy.getCall(0).args[0];
        expect(spy.calledOnce).to.be.true;
        expect(args).to.have.property('method', 'GET');
        expect(args).to.have.property('url', 'https://test.net/api/REST/1.0/data/contacts');
        expect(args).to.have.deep.property('qs.search', 'email=test@domo.com');
        done();
      });
    });
  });

  describe('.getObjectFields', () => {
    beforeEach(done => {
      nock('https://test.net').get('/api/REST/2.0/assets/customObject/test?depth=complete').reply(200, { fields: [] });
      done();
    });

    it('should create formatted request', done => {
      service.getObjectFields('test').then(fields => {
        const args = spy.getCall(0).args[0];
        expect(fields).to.exist;
        expect(spy.calledOnce).to.be.true;

        expect(args).to.have.property('method', 'GET');
        expect(args).to.have.property('url', 'https://test.net/api/REST/2.0/assets/customObject/test?depth=complete');
        expect(args).to.have.deep.property('auth.username', 'company\\usr');
        expect(args).to.have.deep.property('auth.password', 'pwd');
        done();
      });
    });
  });

  describe('.importDataFromStyx', () => {
    let expectedLead;
    beforeEach(done => {
      expectedLead = new AppstoreLead(TestData.lead);
      nock('https://test.net').post('/api/REST/2.0/data/customObject/test/instance').reply(201);
      done();
    });

    it('should call .getContactByEmail', done => {
      const contactSpy = sinon.stub(service, 'getContactByEmail').returns(Promise.resolve({ elements: [{ id: 'test' }] }));
      service.importDataFromStyx('test', TestData.fields, [TestData.lead]).then(() => {
        expect(contactSpy.calledOnce).to.be.true;
        done();
      });
    });

    it('should create formatted request', done => {
      sinon.stub(service, 'getContactByEmail').returns(Promise.resolve({ elements: [{ id: 'test' }] }));
      service.importDataFromStyx('test', TestData.fields, [TestData.lead]).then(res => {
        const args = spy.getCall(0).args[0];

        expect(res).to.exist;
        expect(res).to.an.instanceOf(Array);
        expect(res.length).to.equal(1);
        expect(spy.calledOnce).to.be.true;

        expect(args).to.have.property('method', 'POST');
        expect(args).to.have.property('url', 'https://test.net/api/REST/2.0/data/customObject/test/instance');
        expect(args).to.have.deep.property('auth.username', 'company\\usr');
        expect(args).to.have.deep.property('auth.password', 'pwd');
        expect(args).to.have.deep.property('json.type', 'CustomObjectData');
        expect(args).to.have.deep.property('json.fieldValues');
        expect(args.json.fieldValues).to.have.lengthOf(expectedLead.toEloqua(TestData.fields).length);
        expect(args.json.fieldValues).to.deep.equal(expectedLead.toEloqua(TestData.fields));

        done();
      });
    });
  });
});

const winston = require('winston');
const BasicAPIClient = require('../api-client');
const AppstoreLead = require('../../models/AppstoreLead');

class EloquaAPIClient extends BasicAPIClient {
  constructor(host, company, username, password) {
    super(host);
    this.company = company;
    this.username = username;
    this.authUser = `${this.company}\\${this.username}`;
    this.password = password;

    this.urls = {
      getCustomObj: (parent) => `/api/REST/2.0/assets/customObject/${parent}?depth=complete`,
      getCustomObjData: (parent) => `/api/REST/2.0/data/customObject/${parent}/instances`,
      createCustomObjData: (parent) => `/api/REST/2.0/data/customObject/${parent}/instance`,
      getContact: () => '/api/REST/1.0/data/contacts'
    };
  }

  getContactByEmail(email) {
    const options = {
      method: 'GET',
      url: this.buildURL('getContact'),
      qs: { search: `email=${email}` },
      headers: this.headers,
      auth: { username: this.authUser, password: this.password }
    };

    return this.request(options)
      .then(res => res.elements[0])
      .catch(err => {
        winston.log('error', 'Eloqua: Unable to get Contact By Email', { email, err: err.message });
      });
  }

  getObjectFields(objId) {
    const options = {
      method: 'GET',
      url: this.buildURL('getCustomObj', objId),
      headers: this.headers,
      auth: { username: this.authUser, password: this.password }
    };

    return this.request(options)
      .then(res => res.fields)
      .catch(err => {
        winston.log('error', 'Eloqua: Unable to get Object Fields', { object: objId, err: err.message });
      });
  }

  importDataFromStyx(objId, fields, leads) {
    const promises = leads.map(lead => {
      const appstoreLead = new AppstoreLead(lead);
      const fieldValues = appstoreLead.toEloqua(fields);

      return this.getContactByEmail(appstoreLead.userEmail)
        .then(contact => {
          const options = {
            method: 'POST',
            url: this.buildURL('createCustomObjData', objId),
            json: {
              type: 'CustomObjectData',
              contactId: contact ? contact.id : null,
              fieldValues
            },
            headers: this.headers,
            auth: { username: this.authUser, password: this.password }
          };

          return this.request(options, 201)
            .then(res => {
              winston.log('info', 'Eloqua: Lead imported', { app: lead.appId, user: lead.userEmail });
              return res;
            })
            .catch(err => {
              if (err.status === 400 && err.response[0].type === 'ObjectValidationError' && err.response[0].requirement.type === 'NoDuplicatesRequirement') {
                winston.log('error', 'Duplicate Lead found and will not be imported', { lead: appstoreLead.uniqueId });
              } else {
                winston.log('error', 'Eloqua: Unknown', { err });
              }
            });
        });
    });

    return Promise.all(promises);
  }
}

module.exports = EloquaAPIClient;

const Promise = require('bluebird');
const request = Promise.promisify(require('request'));

class BasicHTTPService {
  constructor(host) {
    this.host = host;
    this.urls = {};
  }

  request(options, statusCode) {
    return request(options).then(res => this.prepareResponse(res, statusCode || 200));
  }

  prepareResponse(res, statusCode) {
    let body;

    try {
      body = JSON.parse(res.body);
    } catch (e) {
      body = res.body;
    }

    if (res.statusCode !== statusCode) {
      const err = new Error(`${res.statusCode} - ${JSON.stringify(res.body)}`);
      err.status = res.statusCode;
      err.response = body;

      throw err;
    }

    return body;
  }

  buildURL(api, urlParam) {
    return `https://${this.host}${this.urls[api](urlParam)}`;
  }
}

module.exports = BasicHTTPService;

class DomoAuthenticationError extends Error {
  constructor(err) {
    const message = err.message;
    super(message);
    this.name = 'DomoAuthentication';
    this.status = 401;
  }
}

class ResourceNotFoundError extends Error {
  constructor(id) {
    const message = `Resource ${id} not found`;
    super(message);
    this.name = 'ResourceNotFound';
    this.status = 400;
  }
}

class AdminAccessRequiredError extends Error {
  constructor() {
    const message = 'Unauthorized to access this resource';
    super(message);
    this.name = 'ResourceAccessDenied';
    this.status = 401;
  }
}

class ResourceAccessError extends Error {
  constructor(id) {
    const message = `Unauthorized to access resource ${id}`;
    super(message);
    this.name = 'ResourceAccessDenied';
    this.status = 401;
  }
}

class ResourceValidationError extends Error {
  constructor(message) {
    super(message);
    this.name = 'ResourceValidationError';
    this.status = 400;
  }
}

class ResourceGeneralError extends Error {
  constructor(message) {
    super(message);
    this.name = 'ResourceGeneralError';
    this.status = 500;
  }
}

module.exports = {
  DomoAuthenticationError,
  ResourceNotFoundError,
  AdminAccessRequiredError,
  ResourceAccessError,
  ResourceValidationError,
  ResourceGeneralError
};

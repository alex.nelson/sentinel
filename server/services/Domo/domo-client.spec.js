/* eslint-disable no-unused-expressions */
import sinon from 'sinon';
import { expect } from 'chai';
import Domo from './domo-client';
import nock from 'nock';

describe('(Service): Domo', () => {
  const domain = 'test.domo.com';
  const sid = 'test-sid';
  const token = 'test-token';
  const host = `https://${domain}`;
  const datasetId = 'test-data-id';
  let domo;

  beforeEach(done => {
    nock(host).get('/api/content/v2/authentication/details').reply(200);
    domo = new Domo(domain, sid, token);
    done();
  });

  it('should instantiate', () => {
    expect(domo).to.be.instanceof(Domo);
  });

  describe('.getDatasetJSON', () => {
    beforeEach(() => {
      nock(host)
        .get(`/api/data/v2/datasources/${datasetId}/dataversions/latest`)
        .query({ accept: 'text/csv', includeHeader: false })
        .reply('col1,col2\nhello,world');
    });

    it('should instantiate', () => {
      expect(domo.getDatasetJSON).to.exist;
    });

    it('should call .getData', () => {
      const spy = sinon.spy(domo, 'getData');
      domo.getDatasetJSON(datasetId).then(() => {
        expect(spy.callCount).to.be.equal(1);
      });
    });
  });
});

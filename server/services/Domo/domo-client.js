const BasicAPIClient = require('../api-client');
const Promise = require('bluebird');
const parse = Promise.promisify(require('csv-parse'));

/* Domo Service
 *
 * Wrapper for the needed Domo endpoints. Any route that needs to consume
 * a Domo endpoint should reference this service instead of building out
 * all of the custom requests multiple times.
 */
class DomoAPIClient extends BasicAPIClient {
  constructor(host, sid, token) {
    super(host);

    this.server = `https://${this.host}`;
    this.sid = sid;
    this.token = token;

    if (this.token) {
      this.headers = { 'x-domo-developer-token': this.token };
    } else {
      this.headers = { 'x-domo-authentication': this.sid };
    }

    this.urls = {
      auth: () => '/api/domoweb/auth/login',
      authDetails: () => '/api/content/v2/authentication/details',
      getDataset: (id) => `/api/data/v3/datasources/${id}`,
      createDataset: () => '/api/data/v2/datasources',
      updateDataset: (id) => `/api/data/v3/datasources/${id}/dataversions`,
      getSchema: (id) => `/api/data/v2/datasources/${id}/schemas/latest`,
      getData: (id) => `/api/data/v2/datasources/${id}/dataversions/latest`,
      apps: () => '/api/content/v1/pages/navigation',
      validateApp: () => '/api/library/v1/apps/validate',
    };
  }

  /* Domo Authentication */
  login(username, password) {
    const options = {
      url: this.buildURL('auth'),
      method: 'POST',
      json: {
        username,
        password: new Buffer(password).toString('base64'),
        base64: true
      }
    };

    return this.request(options);
  }

  /* Ensure Session ID is valid */
  validateSession() {
    const options = {
      url: this.buildURL('authDetails'),
      method: 'GET',
      headers: this.headers
    };

    return this.request(options);
  }

  /* Return dataset definition by ID */
  getDataset(dataset) {
    const options = {
      url: this.buildURL('getDataset', dataset),
      method: 'GET',
      headers: this.headers
    };

    return this.request(options);
  }

  /* Returns a list of pages user has access to */
  getApps(includeHidden) {
    const options = {
      url: this.buildURL('userPages'),
      method: 'GET',
      params: { includeHidden },
      headers: this.headers
    };

    return this.request(options);
  }

  /* Returns the Appstore validation results */
  validateApp(id) {
    let pageId = id;
    if (typeof id !== Number) { pageId = Number(id); }

    const options = {
      url: this.buildURL('validateApp'),
      method: 'GET',
      qs: { pageId },
      headers: this.headers
    };

    return this.request(options);
  }

  /* Returns dataset schema by Id */
  getSchema(dataset) {
    const options = {
      url: this.buildURL('getSchema', dataset),
      method: 'GET',
      headers: this.headers
    };

    return this.request(options);
  }

  /* Returns dataset columns by Id */
  getSchemaColumns(dataset) {
    return this.getSchema(dataset).then(res => {
      const schema = (typeof res === 'string') ? JSON.parse(res) : res;
      return schema.columns;
    });
  }

  /* Returns the latest data version */
  getData(dataset) {
    const options = {
      url: this.buildURL('getData', dataset),
      method: 'GET',
      qs: { accept: 'text/csv', includeHeader: false },
      headers: this.headers
    };

    return this.request(options);
  }

  getDatasetJSON(dataset) {
    return this.getData(dataset)
      .then(data => {
        // remove bad characters
        const input = data.replace(/\\N/g, '""');
        return parse(input, { columns: true, trim: true, auto_parse: true, skip_empty_lines: true });
      });
  }

  /* Create a new webform */
  createDataset(dataset, schema) {
    const options = {
      url: this.buildURL('createDataset'),
      method: 'POST',
      json: {
        dataSourceName: dataset,
        dataSourceType: 'Webform',
        dataProviderType: 'Webform',
        connectorId: 'com.domo.connector.webForm',
        schema
      },
      headers: this.headers
    };
    return this.request(options);
  }

  /* Upload csv data into specified dataset */
  updateDataset(dataset, data) {
    this.headers['Content-Type'] = 'text/csv';
    const options = {
      url: this.buildURL('updataDataset', dataset),
      method: 'POST',
      data,
      headers: this.headers
    };
    return this.request(options);
  }
}

module.exports = DomoAPIClient;

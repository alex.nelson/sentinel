/* eslint-disable no-unused-expressions */
import { expect } from 'chai';
import Model from './appstore-lead.model';
import TestData from '../../../static/testdata/leads.json';

describe('(Model): AppstoreLead', () => {
  let lead;
  beforeEach(() => {
    lead = new Model(TestData.lead);
  });

  it('should instantiate', (done) => {
    expect(Model).to.exist;
    done();
  });

  it('should instantiate from styx', (done) => {
    expect(lead).to.exist;
    expect(lead).to.be.an.instanceOf(Model);

    Object.keys(lead).forEach(key => {
      if (['appDescription', 'firstName', 'lastName', 'uniqueId'].indexOf(key) === -1) {
        expect(lead[key]).to.equal(TestData.lead[key]);
      }
    });

    done();
  });

  it('should append header to description', done => {
    expect(lead.appDescription).to.not.equal(TestData.lead.appDescription);
    expect(lead.appDescription).to.have.string('For app pricing and publisher contact information, see the "Apps: List Price & Hours" card in Domo.');
    expect(lead.appDescription).to.have.string('For partner-built apps, please include the partner in the sales process.');
    done();
  });

  it('should export eloqua custom object', done => {
    const eloquaLead = lead.toEloqua(TestData.fields);

    expect(eloquaLead).to.exist;
    expect(eloquaLead.length).to.equal(TestData.fields.length);

    TestData.fields
      .filter(field => field.dataType !== 'date')
      .forEach(field => {
        const found = eloquaLead.filter(col => col.id === field.id);
        expect(found.length).to.equal(1);
        expect(found[0].value).to.equal(lead[field.name]);
      });

    done();
  });

  it('should parse user name', done => {
    expect(lead.firstName).to.equal('Darth');
    expect(lead.lastName).to.equal('Vader');
    done();
  });

  it('should create unique id', done => {
    expect(lead.uniqueId).to.equal(`${lead.userEmail}_${lead.appId}`);
    done();
  });

  it('should convert millisecond timestamp to second timestamp', done => {
    const eloquaLead = lead.toEloqua(TestData.fields);
    const dateField = TestData.fields.filter(f => f.name === 'requestDate')[0];
    const field = eloquaLead.filter(r => r.id === dateField.id)[0];
    expect(field.value).to.equal(Math.floor(lead.requestDate / 1000));
    done();
  });
});

class AppstoreLead {
  constructor(styxLead) {
    this.appId = styxLead.appId;
    this.appTitle = styxLead.appTitle;
    this.requestDate = styxLead.requestDate;
    this.userEmail = styxLead.userEmail;
    this.userContactNumber = styxLead.userContactNumber;
    this.customerName = styxLead.customerName;
    this.customerHostName = styxLead.customerHostName;
    this.publisherName = styxLead.publisherName;
    this.publisherHostName = styxLead.publisherHostName;
    this.publisherCompanyName = styxLead.publisherCompanyName;
    this.environmentId = styxLead.environmentId;
    this.uniqueId = `${this.userEmail}_${this.appId}`;

    this._setDescription(styxLead.appDescription);
    this._parseName(styxLead.userName);
  }

  _setDescription(desc) {
    this.appDescription = `
      ***
        For app pricing and publisher contact information, see the "Apps: List Price & Hours" card in Domo.
        For partner-built apps, please include the partner in the sales process.
      ***

      ${desc}
    `;
  }

  _parseName(fullname) {
    if (!fullname) return;

    this.userName = fullname;
    const delimiter = fullname.indexOf(',') > -1 ? ',' : ' ';
    const nameParts = fullname.split(delimiter);

    this.firstName = delimiter === ',' ? nameParts[nameParts.length - 1] : nameParts[0];
    this.lastName = delimiter === ',' ? nameParts[0] : nameParts[nameParts.length - 1];
  }

  toEloqua(fields) {
    const dataTypes = {
      largeText: (value) => (value || ''),
      text: (value) => (value || ''),
      date: (value) => Math.floor(value / 1000)
    };

    return fields
      .filter(field => Object.keys(this).indexOf(field.name) > -1)
      .map(field => ({
        id: field.id,
        value: dataTypes[field.dataType](this[field.name])
      }));
  }
}

module.exports = AppstoreLead;

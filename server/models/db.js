const fs = require('fs');
const mongoose = require('mongoose');
mongoose.Promise = require('bluebird');

require('./UpdateLog');

const connectionStrings = {
  test: 'mongodb://localhost:27017/test',
  development: 'mongodb://localhost:27017/dev',
  production: `mongodb://${process.env.MONGO_USR}:${process.env.MONGO_PWD}@aws-us-east-1-portal.21.dblayer.com:10462,aws-us-east-1-portal.22.dblayer.com:10465/sentinel?ssl=true`
};

const options = {};
if (process.env.NODE_ENV === 'production') {
  const sslCA = fs.readFileSync(process.env.MONGO_CA_PATH);
  options.mongos = { ssl: true, sslValidate: false, sslCA };
}

module.exports = {
  db: connectionStrings[process.env.NODE_ENV || 'development'],
  options,
};


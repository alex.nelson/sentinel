const mongoose = require('mongoose');
const Schema = mongoose.Schema;

const schema = new Schema({
  name: String,
  domain: {
    type: String,
    required: [true, 'Publisher domain is required'],
    index: { unique: true }
  },
  contact: {
    name: String,
    email: String
  },
  isDeleted: { type: Boolean, default: false }
}, { timestamps: true });

module.exports = schema;

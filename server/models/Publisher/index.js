import mongoose from 'mongoose';
import Schema from './model';

let Model;

try {
  Model = mongoose.model('Publisher');
} catch (err) {
  Model = mongoose.model('Publisher', Schema);
}

module.exports = Model;

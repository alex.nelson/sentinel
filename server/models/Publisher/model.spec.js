/* eslint-disable no-unused-expressions */
import mongoose from 'mongoose';
import Promise from 'bluebird';
import { expect } from 'chai';
import Schema from './model';

mongoose.Promise = Promise;

describe('(Model): Publisher', () => {
  let connection;
  let Model;

  beforeEach((done) => {
    connection = mongoose.createConnection(process.env.MONGO_URL || 'mongodb://127.0.0.1:27017/test');
    connection.once('open', () => {
      connection.db.dropDatabase(() => {
        Model = connection.model('Publisher', Schema); // wait for mongoose to build schema index
        Model.on('index', () => done());
      });
    });
  });

  afterEach((done) => {
    connection.close(done);
  });

  it('should instantiate', (done) => {
    expect(Model.modelName).to.equal('Publisher');
    done();
  });

  it('should require domain', (done) => {
    Model
      .insertMany([{}])
      .then(res => {
        expect(res).to.not.exist;
      })
      .catch(err => {
        expect(err).to.exist;
        expect(err.name).to.equal('ValidationError');
        expect(err.errors.domain).to.exist;
        expect(err.errors.domain.kind).to.equal('required');
      })
      .finally(() => done());
  });

  it('should require unique domain', (done) => {
    Model
      .insertMany([{ domain: 'test' }, { domain: 'test' }])
      .then(res => {
        expect(res).to.not.exist;
      })
      .catch(err => {
        expect(err).to.exist;
        expect(err.name).to.equal('MongoError');
        expect(err.code).to.equal(11000);
      })
      .finally(() => done());
  });
});

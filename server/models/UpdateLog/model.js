const mongoose = require('mongoose');
const Schema = mongoose.Schema;

const schema = new Schema({
  app: String,
  startTime: Number,
  finishTime: Number,
  responseTime: Number,
  downloaded: Number,
  inserted: Number
});

schema
  .pre('save', preSave)
  .statics.getLastLog = getLastLog;

module.exports = schema;

function preSave(next) {
  this.finishTime = new Date().getTime();
  this.responseTime = this.finishTime - this.startTime;
  next();
}

function getLastLog(app) {
  return this.find({ app })
    .sort({ startTime: -1 })
    .limit(1)
    .then(([log]) => log);
}

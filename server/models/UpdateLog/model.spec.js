/* eslint-disable no-unused-expressions */
import mongoose from 'mongoose';
import Promise from 'bluebird';
import { expect } from 'chai';
import Schema from './model';

mongoose.Promise = Promise;

describe('(Model): UpdateLog', () => {
  let connection;
  let Model;

  beforeEach((done) => {
    connection = mongoose.createConnection(process.env.MONGO_URL || 'mongodb://127.0.0.1:27017/test');
    connection.once('open', () => {
      connection.db.dropDatabase(() => {
        Model = connection.model('UpdateLog', Schema); // wait for mongoose to build schema index
        done();
      });
    });
  });

  afterEach((done) => {
    connection.close(done);
  });

  it('should instantiate', (done) => {
    expect(Model.modelName).to.equal('UpdateLog');
    done();
  });

  it('should calculate response time on save', done => {
    const wait = 500;
    const log = new Model();
    log.startTime = new Date().getTime();

    setTimeout(() => {
      log.app = 'test';
      log.downloaded = 100;
      log.inserted = 100;

      log.save().then(l => {
        expect(l).to.exist;
        expect(l.app).to.equal('test');
        expect(l.downloaded).to.equal(100);
        expect(l.inserted).to.equal(100);
        expect(l.finishTime).to.be.above(l.startTime + wait);
        expect(l.responseTime).to.exist;
        expect(l.responseTime).to.be.equal(l.finishTime - l.startTime);

        done();
      });
    }, wait);
  });

  it('.getLastLog should return top entry', done => {
    const lastDate = new Date();

    Model
      .insertMany([
        { app: 'test', startTime: new Date(2017, 0, 1).getTime(), downloaded: 1, inserted: 1 },
        { app: 'test', startTime: new Date(2017, 1, 1).getTime(), downloaded: 2, inserted: 1 },
        { app: 'test', startTime: new Date(2017, 2, 1).getTime(), downloaded: 3, inserted: 1 },
        { app: 'test', startTime: lastDate.getTime(), downloaded: 4, inserted: 1 }
      ])
      .then(() => Model.getLastLog('test'))
      .then(log => {
        expect(log).to.exist;
        expect(log).to.be.an.instanceOf(Model);
        expect(log.app).to.equal('test');
        expect(log.startTime).to.equal(lastDate.getTime());
        expect(log.downloaded).to.equal(4);
        expect(log.inserted).to.equal(1);

        done();
      });
  });
});

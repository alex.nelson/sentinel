import mongoose from 'mongoose';
import Schema from './model';

let Model;

try {
  Model = mongoose.model('UpdateLog');
} catch (err) {
  Model = mongoose.model('UpdateLog', Schema);
}

module.exports = Model;

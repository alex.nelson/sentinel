const ERRORS = {
  ValidationError: 400,
  ValidatorError: 400,
  MongoError: 400
};

module.exports = () => {
  return {
    global: (err, req, res, next) => {
      const status = err.status ? err.status : ERRORS[err.name] || 500;
      res.status(status).json({
        source: req.url,
        name: err.name,
        message: err.message,
        errors: err.errors,
        status
      });

      // Log errors to console
      if (err.status === 500) {
        const date = new Date().getTime();
        process.stdout.write((`\n\n${date}: ERROR: ${req.url}: ${err.name}: ${err.message}\n`));
      }

      next();
    }
  };
};

/* eslint-disable no-unused-expressions */
import { expect } from 'chai';
const errorMiddleware = require('./error.middleware')();

describe('(Middleware): Errors', () => {
  it('should instantiate', done => {
    expect(errorMiddleware).to.exist;
    done();
  });

  it('should instantiate global', done => {
    expect(errorMiddleware.global).to.exist;
    expect(errorMiddleware.global).to.be.instanceOf(Function);
    done();
  });
});

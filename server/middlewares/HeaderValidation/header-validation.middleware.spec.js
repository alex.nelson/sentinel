/* eslint-disable no-unused-expressions */
import { expect } from 'chai';
const headerMiddleware = require('./header-validation.middleware')();

describe('(Middleware): Authentication', () => {
  it('should instantiate', done => {
    expect(headerMiddleware).to.exist;
    done();
  });

  it('should instantiate request', done => {
    expect(headerMiddleware.request).to.exist;
    expect(headerMiddleware.request).to.be.instanceOf(Function);
    done();
  });

  it('should instantiate response', done => {
    expect(headerMiddleware.response).to.exist;
    expect(headerMiddleware.response).to.be.instanceOf(Function);
    done();
  });

  it('should instantiate proxy', done => {
    expect(headerMiddleware.proxy).to.exist;
    expect(headerMiddleware.proxy).to.be.instanceOf(Function);
    done();
  });
});

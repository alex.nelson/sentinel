module.exports = () => {
  return {
    request: (req, res, next) => {
      const missingSid = !req.headers.hasOwnProperty('x-session');
      const missingToken = !req.headers.hasOwnProperty('x-token');
      const loginUrl = req.url.indexOf('auth/login') > -1;

      const err = new Error();
      err.status = 400;
      err.name = 'MissingHeader';

      if (!req.headers.hasOwnProperty('x-domo')) {
        err.message = 'Missing required "x-domo" header';
        next(err);
      } else if (!loginUrl && missingSid && missingToken) {
        err.message = 'Missing required "x-session" or "x-token" header';
        next(err);
      } else {
        // Default to active objects
        req.preparedQuery = { isDeleted: false };

        Object.keys(req.query).forEach(key => {
          if (req.query[key] !== 'true' && req.query[key] !== 'false') {
            req.preparedQuery[key] = new RegExp(`.*${req.query[key]}.*`, 'i');
          } else {
            req.preparedQuery[key] = (req.query[key] === 'true');
          }
        });
        next();
      }
    },
    response: (req, res, next) => {
      res.set('access-control-allow-origin', '*');
      res.header('Access-Control-Allow-Methods', 'GET,POST,PUT,OPTIONS');
      res.header('Access-Control-Allow-Headers', 'x-domo, x-session, x-token, content-type, accept');

      // intercept OPTIONS method
      if (req.method === 'OPTIONS') { res.sendStatus(200); } else { next(); }
    },
    proxy: (req, res, next) => {
      const domain = req.headers['x-domo'];
      const token = req.headers['x-token'];
      const session = req.headers['x-session'];

      const api = req.params[0];

      if (!domain || !api) {
        const message = !api ? 'Missing url to proxy' : 'Missing required x-domo header';

        res.status(400).json({ status: 400, message });
      } else {
        const keys = Object.keys(req.headers);

        const headers = {};
        if (session) headers['x-domo-authentication'] = session;
        if (token) headers['x-domo-developer-token'] = token;

        keys.forEach(key => {
          if (key.indexOf('accept') > -1) headers[key] = req.headers[key];
          if (key.indexOf('type') > -1) headers[key] = req.headers[key];
        });

        req._headers = headers;

        next();
      }
    }
  };
};

import Domo from '../../services/Domo';
import { DomoAuthenticationError } from '../../services/Errors';

const adminString = process.env.DOMO_ADMIN || 'domo.domo.com,domo-sentinel.beta.domo.com';
const DOMO_ADMIN = adminString.split(',');

module.exports = () => {
  return {
    basic: (req, res, next) => {
      if (req.url.indexOf('/auth') === -1 && req.url.indexOf('/admin') === -1) {
        const domo = new Domo(req.headers['x-domo'], req.headers['x-session'], req.headers['x-token']);

        return domo
          .validateSession()
          .then(() => {
            next();
            return null;
          })
          .catch(reason => {
            const err = new DomoAuthenticationError(reason);
            next(err);
          });
      }

      return next();
    },
    publisher: (Publisher) => (req, res, next) => {
      const domo = new Domo(req.headers['x-domo'], req.headers['x-session'], req.headers['x-token']);

      return Publisher
        .findOne({ _id: req.params.publisher, domain: domo.host })
        .then(publisher => {
          req.publisher = publisher;
          return next();
        })
        .catch(reason => {
          const err = new DomoAuthenticationError(reason);
          next(err);
        });
    },
    admin: (req, res, next) => {
      if (DOMO_ADMIN.indexOf(req.headers['x-domo']) === -1) {
        const err = new DomoAuthenticationError({ message: 'Unauthorized to access this resource' });
        next(err);
      } else {
        req.isAdmin = true;
        next();
      }
    }
  };
};

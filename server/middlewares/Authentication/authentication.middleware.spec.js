/* eslint-disable no-unused-expressions */
import { expect } from 'chai';
const authMiddleware = require('./authentication.middleware')();

describe('(Middleware): Authentication', () => {
  it('should instantiate', done => {
    expect(authMiddleware).to.exist;
    done();
  });

  it('should instantiate basic', done => {
    expect(authMiddleware.basic).to.exist;
    expect(authMiddleware.basic).to.be.instanceOf(Function);
    done();
  });

  it('should instantiate publisher', done => {
    expect(authMiddleware.publisher).to.exist;
    expect(authMiddleware.publisher).to.be.instanceOf(Function);
    done();
  });

  it('should instantiate admin', done => {
    expect(authMiddleware.admin).to.exist;
    expect(authMiddleware.admin).to.be.instanceOf(Function);
    done();
  });
});

# Route Middleware

These functions are called before or after an Express route. They're meant
to perform any necessary validations or transformations that are common across
multiple routes.

## admin-auth
In addition to performing the basic auth, will block any request where the authenticated Domo instance is not listed as an "Admin" instance.

## basic-auth
Ensures that each request has a valid SID or Dev Token sent as a header

## domo-response
Helper function to validate any responses from a Domo API

## error
General error handling

## headers
Set default response headers

## request
Validating and blocking any request that doesn't have the required headers

# App Routes

## content
Routes that communicate only with the Sentinel DB

## content > admin
A subset of content routes that require elevated access

## domo
Routes that proxy requests to the provided Domo instance

## tools
Utility routes that don't necessarily communicate with the DB or Domo

const Promise = require('bluebird');
const winston = require('winston');

module.exports = (UpdateLog) => {
  return {
    leads: (EloquaAPI, StyxAPI) => (req, res, next) => {
      const log = new UpdateLog();
      log.app = 'eloqua';
      log.startTime = new Date().getTime();

      winston.log('info', 'Update Appstore Leads', req.body);

      return UpdateLog
        .getLastLog(log.app)
        .then(lastLog => {
          return Promise.all([
            StyxAPI.getNewLeads(lastLog ? lastLog.startTime : log.startTime, req.body.envIds),
            EloquaAPI.getObjectFields(req.body.objectId)
          ]);
        })
        .then(([leads, fields]) => {
          log.downloaded = leads.length;
          return EloquaAPI.importDataFromStyx(req.body.objectId, fields, leads);
        })
        .then(leads => {
          log.inserted = leads.filter(lead => lead && lead.id !== '0').length;
          return log.save();
        })
        .then(newLog => { res.json(newLog); })
        .catch(err => { next(err); });
    }
  };
};

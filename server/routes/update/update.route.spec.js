/* eslint-disable no-unused-expressions */
import sinon from 'sinon';
import { expect } from 'chai';
import mongoose from 'mongoose';
import Promise from 'bluebird';
import UpdateSchema from '../../models/UpdateLog/model';
import routes from './update.route';

mongoose.Promise = Promise;

describe('(Route): Update', () => {
  let connection;
  let UpdateRoutes;
  let Model;
  const res = { json: () => {} };

  beforeEach((done) => {
    sinon.spy(res, 'json');
    connection = mongoose.createConnection(process.env.MONGO_URL || 'mongodb://127.0.0.1:27017/test');
    connection.once('open', () => {
      connection.db.dropDatabase(() => {
        Model = connection.model('Update', UpdateSchema); // wait for mongoose to build schema index
        UpdateRoutes = routes(Model);
        done();
      });
    });
  });

  afterEach((done) => {
    connection.close(done);
    res.json.restore();
  });

  it('should instantiate', done => {
    expect(routes).to.exist;
    done();
  });

  describe('.leads', () => {
    it('should instantiate', done => {
      expect(UpdateRoutes.leads).to.exist;
      done();
    });

    it('should return promise', done => {
      const promise = UpdateRoutes.leads(sinon.spy(), sinon.spy())({ body: {} }, res, () => {});
      expect(promise).to.be.instanceOf(Promise);
      done();
    });

    it('should get last log', done => {
      const modelSpy = sinon.spy(Model, 'getLastLog');

      UpdateRoutes
        .leads(sinon.spy(), sinon.spy())({ body: {} }, res, () => {})
        .then(() => {
          expect(modelSpy.calledOnce).to.be.true;
          done();
        });
    });

    it('should call service APIs', done => {
      const modelSpy = sinon
        .stub(Model, 'getLastLog')
        .returns(Promise.resolve(null));

      const eloquaStub = {
        getObjectFields: sinon.stub().returns(Promise.resolve([])),
        importDataFromStyx: sinon.stub().returns(Promise.resolve())
      };

      const styxStub = {
        getNewLeads: sinon.stub().returns(Promise.resolve([]))
      };

      UpdateRoutes
        .leads(eloquaStub, styxStub)({ body: {} }, res, () => {})
        .then(() => {
          expect(modelSpy.calledOnce).to.be.true;
          expect(eloquaStub.getObjectFields.calledOnce).to.be.true;
          expect(eloquaStub.importDataFromStyx.calledOnce).to.be.true;
          expect(styxStub.getNewLeads.calledOnce).to.be.true;
          done();
        });
    });
  });
});

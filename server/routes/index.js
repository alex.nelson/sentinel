const authMiddlewares = require('../middlewares/Authentication')();
const headerMiddlewares = require('../middlewares/HeaderValidation')();
const errorMiddlewares = require('../middlewares/Errors')();

const authRoutes = require('./auth')();
const proxyRoutes = require('./proxy')();
const healthRoutes = require('./health')();
const updateRoutes = require('./update')(require('../models/UpdateLog'));

const EloquaAPI = require('../services/Eloqua');
const StyxAPI = require('../services/Styx');

const eloquaHostName = process.env.ELOQUA_HOSTNAME || 'secure.p01.eloqua.com';
const eloquaCompanyName = process.env.ELOQUA_COMPANY;
const eloquaUserName = process.env.ELOQUA_USERNAME;
const eloquaPassword = process.env.ELOQUA_PASSWORD;

const adminConsoleHost = process.env.ADMIN_CONSOLE_HOSTNAME;
const adminConsoleUser = process.env.ADMIN_CONSOLE_USERNAME;
const adminConsolePassword = process.env.ADMIN_CONSOLE_PASSWORD;

const eloquaAPI = new EloquaAPI(eloquaHostName, eloquaCompanyName, eloquaUserName, eloquaPassword);
const styxAPI = new StyxAPI(adminConsoleHost, adminConsoleUser, adminConsolePassword);

module.exports = (app) => {
  // Global Middleware
  app.use(headerMiddlewares.response);
  app.use('/api', headerMiddlewares.request);
  app.use('/api', authMiddlewares.basic);

  // Auth
  app.post('/api/auth/login', authRoutes.login);
  app.post('/api/auth/validate', authRoutes.validate);

  // Updates
  app.use('/api/updates/', authMiddlewares.admin);
  app.post('/api/updates/leads', updateRoutes.leads(eloquaAPI, styxAPI));

  // Domo Proxy
  app.use('/proxy/*', headerMiddlewares.proxy);
  app.all('/proxy/*', proxyRoutes.proxy);

  // Server Status
  app.use('/utils/health/sentinel', healthRoutes.sentinel);

  app.use(errorMiddlewares.global);
};

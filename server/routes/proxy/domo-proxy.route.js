const request = require('request');

module.exports = () => {
  return {
    proxy: (req, res) => {
      const domain = req.headers['x-domo'];
      const api = req.params[0];

      const options = {
        url: `https://${domain}/${api}`,
        method: req.method,
        headers: req._headers,
      };

      if (req.method === 'POST' || req.method === 'PUT') {
        const isJson = options.headers['content-type'] === 'application/json';
        options[isJson ? 'json' : 'body'] = req.body;
      }

      // any request type can have query parameters
      if (req.query) {
        options.qs = req.query;
      }

      request(options).pipe(res);
    }
  };
};

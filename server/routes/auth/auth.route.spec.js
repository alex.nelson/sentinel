/* eslint-disable no-unused-expressions */
import Promise from 'bluebird';
import sinon from 'sinon';
import { expect } from 'chai';
import Domo from '../../services/Domo';
const authRoutes = require('./auth.route')();

describe('(Route): Auth', () => {
  let next;
  const req = { headers: { 'x-domo': '' }, body: { username: '', password: '' } };
  const res = { send: () => {} };

  beforeEach(() => {
    sinon.spy(res, 'send');
    next = sinon.spy();
  });

  afterEach(() => {
    res.send.restore();
  });

  it('should instantiate', () => {
    expect(authRoutes).to.exist;
  });

  describe('auth.login', () => {
    it('should instantiate', () => {
      expect(authRoutes.login).to.be.instanceof(Function);
    });

    it('should call domo.login', () => {
      const spy = sinon.stub(Domo.prototype, 'login').returns(Promise.resolve({}));

      authRoutes.login(req, res, next);
      expect(spy.callCount).to.be.equal(1);
      spy.restore();
    });

    it('should handle domo.login success', () => {
      const spy = sinon.stub(Domo.prototype, 'login').returns(Promise.resolve());

      authRoutes.login(req, res, next).then(() => {
        expect(res.send.callCount).to.be.equal(1);
        expect(next.callCount).to.be.equal(0);
        spy.restore();
      });
    });

    it('should handle domo.login errors', () => {
      const spy = sinon.stub(Domo.prototype, 'login').returns(Promise.reject());

      authRoutes.login(req, res, next).catch(() => {
        expect(res.send.callCount).to.be.equal(0);
        expect(next.callCount).to.be.equal(1);
        spy.restore();
      });
    });
  });

  describe('auth.validate', () => {
    it('should validate', () => {
      expect(authRoutes.validate).to.be.instanceof(Function);
    });

    it('should call domo.validateSession', () => {
      const spy = sinon.stub(Domo.prototype, 'validateSession').returns(Promise.resolve({}));

      authRoutes.validate(req, res, next);
      expect(spy.callCount).to.be.equal(1);
      spy.restore();
    });

    it('should handle domo.validateSession success', () => {
      const spy = sinon.stub(Domo.prototype, 'validateSession').returns(Promise.resolve());

      authRoutes.validate(req, res, next).then(() => {
        expect(res.send.callCount).to.be.equal(1);
        expect(next.callCount).to.be.equal(0);
        spy.restore();
      });
    });

    it('should handle domo.validateSession errors', () => {
      const spy = sinon.stub(Domo.prototype, 'validateSession').returns(Promise.reject());

      authRoutes.validate(req, res, next).catch(() => {
        expect(res.send.callCount).to.be.equal(0);
        expect(next.callCount).to.be.equal(1);
        spy.restore();
      });
    });
  });
});

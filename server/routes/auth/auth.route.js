const Domo = require('../../services/Domo');

module.exports = () => {
  return {
    login: (req, res, next) => {
      const domo = new Domo(req.headers['x-domo'], '');

      return domo.login(req.body.username, req.body.password)
        .then(response => res.send(response))
        .catch(err => next(err));
    },
    validate: (req, res, next) => {
      const domo = new Domo(req.headers['x-domo'], req.headers['x-session']);

      return domo.validateSession()
        .then(response => res.send(response))
        .catch(err => next(err));
    }
  };
};

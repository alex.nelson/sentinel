/* eslint-disable no-unused-expressions */
import sinon from 'sinon';
import { expect } from 'chai';
import mongoose from 'mongoose';
import Promise from 'bluebird';
import routes from './health.route';

mongoose.Promise = Promise;

describe('(Route): Health', () => {
  let connection;
  const res = { json: () => {} };

  beforeEach((done) => {
    sinon.spy(res, 'json');
    connection = mongoose.createConnection(process.env.MONGO_URL || 'mongodb://127.0.0.1:27017/test');
    connection.once('open', () => {
      connection.db.dropDatabase(() => {
        done();
      });
    });
  });

  afterEach((done) => {
    connection.close(done);
    res.json.restore();
  });

  it('should instantiate', done => {
    expect(routes).to.exist;
    done();
  });
});


module.exports = () => {
  return {
    sentinel: (req, res) => {
      return res.json({
        message: 'Sentinel Ready',
        status: 200
      });
    }
  };
};

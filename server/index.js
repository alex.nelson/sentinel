const mongoose = require('mongoose');
const express = require('express');
const bodyParser = require('body-parser');
const logger = require('morgan');
const config = require('./models/db');
const winston = require('winston');
winston.level = process.env.LOG_LEVEL || 'debug';

const app = express();
const port = process.env.PORT || 3000;

if (process.env.NODE_ENV === 'development') app.use(logger('dev'));

app.use(bodyParser.urlencoded({ extended: true }));
app.use(bodyParser.text({ type: 'text/*' }));
app.use(bodyParser.json());

// mount routes && middleware
require('./routes')(app);

connect()
  .on('error', process.stdout.write)
  .on('disconnected', connect)
  .once('open', listen);

function listen() {
  if (app.get('env') === 'test') return;

  app.listen(port, '0.0.0.0', '', () => {
    winston.log('info', 'Sentinel Ready');
  });
}

function connect() {
  return mongoose.connect(config.db, config.options).connection;
}

module.exports = app;
